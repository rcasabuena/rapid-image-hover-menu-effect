import React from 'react';
import DemoProps from '../interfaces/DemoProps';

function Demo3(props: DemoProps) {
  return (
    <div>
      <p>Demo 3</p>
    </div>
  )
}

export default Demo3;