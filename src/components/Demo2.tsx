import React from 'react';
import DemoProps from '../interfaces/DemoProps';

function Demo2(props: DemoProps) {
  return (
    <div>
      <p>Demo 2</p>
    </div>
  )
}

export default Demo2;
