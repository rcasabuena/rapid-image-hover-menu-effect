import React from 'react';
import DemoProps from '../interfaces/DemoProps';

function Demo4(props: DemoProps) {
  return (
    <div>
      <p>Demo 4</p>
    </div>
  )
}

export default Demo4;