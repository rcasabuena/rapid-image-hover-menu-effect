import React from 'react';
import DemoProps from '../interfaces/DemoProps';

function Demo1(props: DemoProps) {
  return (
    <div>
      <p>Demo 1</p>
    </div>
  )
}

export default Demo1;
