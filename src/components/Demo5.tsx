import React from 'react';
import DemoProps from '../interfaces/DemoProps';

function Demo5(props: DemoProps) {
  return (
    <div>
      <p>Demo 5</p>
    </div>
  )
}

export default Demo5;