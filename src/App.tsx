import React from 'react';
import { Router, Link } from '@reach/router';
import Demo1 from './components/Demo1';
import Demo2 from './components/Demo2';
import Demo3 from './components/Demo3';
import Demo4 from './components/Demo4';
import Demo5 from './components/Demo5';

function App() {
  return (
    <div>
      <nav>
        <Link to="/">Home</Link>
        <Link to="/demo1">Demo 1</Link>
        <Link to="/demo2">Demo 2</Link>
        <Link to="/demo3">Demo 3</Link>
        <Link to="/demo4">Demo 4</Link>
        <Link to="/demo5">Demo 5</Link>
      </nav>
      <Router>
        <Demo1 path="demo1" default/> 
        <Demo2 path="demo2" /> 
        <Demo3 path="demo3" /> 
        <Demo4 path="demo4" /> 
        <Demo5 path="demo5" /> 
      </Router> 
    </div>
  );
}

export default App;
